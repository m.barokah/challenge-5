module.exports = (req, res, next) => {
    res.respond = (data = null, message = '', statusCode = 200, status = true) => {
        if (statusCode >= 400) status = false
        res.status(statusCode).json({
            status,
            message,
            data
        })
    }

    res.respondCreated = (data = null, message = 'success create data') => {
        res.respond(data, message, 201)
    }
    res.respondUpdated = (data = null, message = 'success update data') => {
        res.respond(data, message, 200)
    }

    res.respondDeleted = (message = 'success delete data', data = null) => {
        res.respond(data, message, 200)
    }

    res.respondNotFound = ( message = 'data is doesn\'t exist', data = null) => {
        res.respond(data, message, 404)
    }
    res.respondApiNotFound = (message = 'Your URL is not Found', data = null) => {
        res.respond(data, message,  404)
    }

    res.respondBadRequest = (message = 'bad request', data = null) => {
        res.respond(data, message, 400)
    }

    res.respondServerError = (message = 'server error', data = null) => {
        res.respond(data, message, 500)
    }
    res.respondSuccess = (data = null, message = 'success') => {
        res.respond(data, message, 201)
    }

    next()
}