const express = require('express')
const router = express.Router()

const { createUser, getUsers,getUser,updateUser,deleteUser } = require('../controllers/usergame')

router.get('/', getUsers)
router.get('/:id', getUser)
router.post('/', createUser)
router.put('/:id', updateUser)
router.delete('/:id', deleteUser)

module.exports = router