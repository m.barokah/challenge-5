const { Usergame } = require('../models')

createUser = async (req, res) => {
    try {
        let { username, email, password } = req.body

        let UserName = await Usergame.findOne({ where: { username: username } });
        if (UserName)  return res.respondBadRequest(`username with ${username} already exists`)
        
        let UserEmail = await Usergame.findOne({ where: { email: email } });
        if (UserEmail) return res.respondBadRequest(`email with ${email} already exists`)

        let newUser = await Usergame.create({
            username,
            email,
            password
        })

        res.respondCreated(newUser)

    } catch (err) {
        res.respondServerError(err.message)
    }
}

getUsers = async (req, res) => {
    try {
        let Users = await Usergame.findAll({ include: ['biodata', 'history'] });

        res.respondSuccess(Users)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

getUser = async (req, res) => {
    try {
        const User_id = req.params.id

        let User = await Usergame.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find user with id ${User_id}`)

        res.respondSuccess(User)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

updateUser = async (req, res) => {
    try {
        const User_id = req.params.id
        const { username, email, password } = req.body

        let User = await Usergame.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find user with id ${User_id}`)
        
        let query = {
            where: {
                id: User_id
            }
        }
        
        let updated = Usergame.update({
            username,
            email,
            password
        }, query)
        res.respondUpdated(updated)
        
    } catch (err) {
        res.respondServerError(err.message)
    }
}

deleteUser = async (req, res) => {
    try {
        const User_id = req.params.id
        let User = await Usergame.findOne({
            where: {
                id: User_id
            }
        })

        if (!User) return res.respondNotFound(`cant find user with id ${User_id}`)
        
        let deleted = await Usergame.destroy({
            where: {
                id: User_id
            }
        })
        res.respondDeleted(deleted)
    } catch (err) {
        res.respondServerError(err.message)
    }
}

module.exports = {
    createUser,
    getUsers,
    getUser,
    updateUser,
    deleteUser
}